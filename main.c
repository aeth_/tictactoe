#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define X 'X'
#define O 'O'
#define BOARD_DIM 3
#define MAX_STRLEN 256

typedef struct player {
	char * name;
	char symbol;
}player_t;

bool win_sig = false;
bool draw_sig = false;
char board[BOARD_DIM][BOARD_DIM];
player_t p1, p2;
char str_buffer[MAX_STRLEN + 1];

player_t initplayer(char p_symbol) {
	player_t p;
	fgets(str_buffer, MAX_STRLEN + 1, stdin);
	str_buffer[strlen(str_buffer) - 1]='\0'; // remove newline
	p.name = (char*)malloc((strlen(str_buffer)+2)*sizeof(char));
	strcpy(p.name, str_buffer);
	p.symbol = p_symbol;
	return p;
}

void initboard() {
	int i, j;
	for (i = 0; i < BOARD_DIM; i++)
		for (j = 0; j < BOARD_DIM; j++)
			board[i][j] = ' ';
}

void printboard() {
	int i, j;
	for (i = 0; i < BOARD_DIM; i++) // upper border
		printf("   %d", i + 1);
	printf("\n");
	for (i = 0; i < BOARD_DIM; i++) {
		printf("%d ", i + 1); // left border
		for (j = 0; j < BOARD_DIM; j++) {
			if (j != 0)
				printf("|");
			printf(" %c ", board[i][j]);
		}
		if (i != BOARD_DIM - 1)
			for(j = 0; j < BOARD_DIM; j++){
				if (j == 0)
					printf("\n  ---");
				else if (j == BOARD_DIM - 1)
					printf("+---\n");
				else
					printf("+---");
			}
	}
	printf("\n\n");
}

void play(player_t p, int * x, int * y) {
	int i;
	char *part;
	do {
		printf("%s insert x:\n", p.name);
		fgets(str_buffer, MAX_STRLEN + 1, stdin);
		*x = strtol(str_buffer, &part, 10);
		while (*x < 1 || *x > BOARD_DIM) {
			printf("Not valid, retry:\n");
			fgets(str_buffer, MAX_STRLEN + 1, stdin);
			*x = strtol(str_buffer, &part, 10);
		}
		printf("%s insert y:\n", p.name);
		fgets(str_buffer, MAX_STRLEN + 1, stdin);
		*y = strtol(str_buffer, &part, 10);
		while (*x < 1 || *x > BOARD_DIM) {
			printf("Not valid, retry:\n");
			fgets(str_buffer, MAX_STRLEN + 1, stdin);
			*x = strtol(str_buffer, &part, 10);
		}
		*x -= 1;
		*y -= 1;
		if (board[*y][*x] != ' ')
			printf("Cell is already taken, reinsert coordinates!\n");
	} while (board[*y][*x] != ' ');
	board[*y][*x] = p.symbol;
}

bool checkwinner(int x, int y, char symbol) {
	int i, j; 
	int vcont  = 1,
	    hcont  = 1,
	    pdcont = 1,
	    sdcont = 1;
	for (i = 0, j = BOARD_DIM - 1; i < BOARD_DIM && j >= 0; i++, j--) {
		if (i != x && i != y && board[i][i] == symbol)
			pdcont++;
		if (i != x && j != y && board[j][i] == symbol)
			sdcont++;
		if (i != x && board[y][i] == symbol)
			hcont++;
		if (i != y && board[i][x] == symbol)
			vcont++;
	}
	if (vcont == BOARD_DIM || hcont == BOARD_DIM || pdcont == BOARD_DIM || sdcont == BOARD_DIM)
		return true;
	return false;
}


int main() {
	int x = -1, 
	    y = -1,
	    moves = 9;		
	printf("Enter the 1st player's name:\n");
	p1 = initplayer(X);
	printf("Welcome %s!\n", p1.name);
	printf("Your symbol is %c\n", p1.symbol);
	printf("Enter the 2nd player's name:\n");
	p2 = initplayer(O);
	printf("Welcome %s!\n", p2.name);
	printf("Your symbol is %c\n", p2.symbol);
	while (true) {
		system("clear");
		initboard();
		printboard();
		while (moves > 0) {
			play(p1, &x, &y);
			system("clear");
			printboard();
			if (checkwinner(x, y, p1.symbol)) {
				printf("%s wins!\n", p1.name);
				break;
			}
			moves--;
			if (moves == 0)
				break;
			play(p2, &x, &y);
			system("clear");
			printboard();
			if (checkwinner(x, y, p2.symbol)) {
				printf("%s wins!\n", p2.name);
				break;
			}
			moves--;
		}
		if (moves == 0)
			printf("It's a draw :(\n");
		printf("wanna play again? [y/n]\n");
		fgets(str_buffer, MAX_STRLEN + 1, stdin);
		while (str_buffer[0] != 'y' && str_buffer[0] != 'n') {
			printf("invalid input: type \"y\" or \"n\":\n");
			fgets(str_buffer, MAX_STRLEN + 1, stdin);
		}
		if (str_buffer[0] == 'n')
			break;
		moves = 9;
	}
    return 0;
}
